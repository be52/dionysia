package simplex

import chisel3.iotesters._

class directorModuleTests(c: director_module) extends PeekPokeTester(c) {
  // TODO: Define number of decimals of precision

  // Define an toy example
  // Note that data will need to be padded to match accelerator size
  val toy = Array( 2, 1, 1, 0, 0, 18,
                   2, 3, 0, 1, 0, 26,
                   3, 1, 0, 0, 1, 25,
                  -3,-2, 0, 0, 0,  0)
  var pos = 0

  // Send datato the director
  printf("Loading data...\n")
  for (i <- 0 until 4) {
    for (j <- 0 until 6) { 
      poke(c.io.message.bits(i)(j), toy(pos))
      pos = pos + 1
    }
  }
  poke(c.io.message.valid, true)
  step(1)
  while (peek(c.io.busy) == 1) {
    printf("...loading...\n")
    step(1)
  }
  printf("Data loaded....\n")
  printf("Turning off message valid signal\n")
  poke(c.io.message.valid, false)
  step(1)

  printf("Starting simplex\n")
  poke(c.io.start_simplex, true)
  var count = 0
  while(peek(c.io.optimal_value.valid) == 0 && count < 200) {
    step(1)
    printf("--- computing ---\n")
    count += 1
  }
  printf("Optimal value: %f\n", peek(c.io.optimal_value.bits).toFloat)
  poke(c.io.start_simplex, false)
  /*
  step(1)
  printf("Printing row 2\n")
  poke(c.io.print_content, 1)
  poke(c.io.print_content_row, 2)
  step(2)

  printf("Printing row 1\n")
  poke(c.io.print_content, 1)
  poke(c.io.print_content_row, 1)
  step(1)
  */

  /*

  step(1)

  poke(c.io.print_content, 1)
  poke(c.io.print_content_row, 0)
  poke(c.io.message.valid, 0)
  step(2)
  step(2)
  poke(c.io.message.valid, false)

  // Get the contents
  */
}

class director_tester extends ChiselFlatSpec {
  behavior of "director_module"
  backends foreach {backend =>
    it should s"do the director function in $backend" in {
      Driver(() => new director_module, backend)(c => new directorModuleTests(c)) should be (true)
    }
  }
}

object director_tester extends App {
  Driver.execute(args, () => new director_module){ c => new directorModuleTests(c) }
}
