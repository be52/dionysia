package simplex

import chisel3.iotesters._

class actorModuleTests(c: actor_module) extends PeekPokeTester(c) {
  // TODO
}

class actor_tester extends ChiselFlatSpec {
  behavior of "actor_module"
  backends foreach {backend =>
    it should s"do the actor function in $backend" in {
      Driver(() => new actor_module, backend)(c => new actorModuleTests(c)) should be (true)
    }
  }
}

object actor_tester extends App {
  Driver.execute(args, () => new actor_module){ c => new actorModuleTests(c) }
}
