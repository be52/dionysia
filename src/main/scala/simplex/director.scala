package simplex

import chisel3._
import chisel3.util._
import dsptools.numbers.implicits._ 
import chisel3.core.{FixedPoint => FP}

/* Notes: 
  * constraint_n and coefficient_n are fixed, but Dionysia can work on any
    problem that has appropriate padding. For example:
     2,  1,  0, 1, 0, 0, 10,
     0,  2,  1, 0, 1, 0, 20,
     1,  0,  2, 0, 0, 1, 30,
    -3, -4, -5, 0, 0, 0, 00
    and 
     2,  1, 0, 0, 0, 1, 0, 0, 0, 0, 18,
     2,  3, 0, 0, 0, 0, 1, 0, 0, 0, 26,
     3,  1, 0, 0, 0, 0, 0, 1, 0, 0, 25,
     0,  0, 0, 0, 0, 0, 0, 0, 1, 0, 00,
     0,  0, 0, 0, 0, 0, 0, 0, 0, 1, 00,
    -3, -2, 0, 0, 0, 0, 0, 0, 0, 0, 00
    are equivalent problems.
  * row number is assumed to be constraint_n + 1
  * column number (i.e. row size) is assumed to be 
    constraint_n + coefficient_n + 1
*/

class director_module(val W: Int = 64, val constraint_n: Int = 3, val coefficient_n: Int = 2)
extends Module {
  val row_num = constraint_n + 1
  val row_size = constraint_n + coefficient_n + 1

  val io = IO(new Bundle {
    // Message contains the data  
    val message = Flipped(new DecoupledIO(Vec(row_num, Vec(row_size, SInt(W.W)))))

    // Busy indicator
    val busy = Output(Bool())
    // Start indicator
    val start_simplex = Input(Bool())
    // Optimal value output
    val optimal_value = Decoupled(SInt(W.W))

    // Debug commands
    val print_content = Input(Bool())
    val print_content_row = Input(UInt(W.W))
  })

  // Helper functions
  def sum(s1: SInt, s2: SInt): SInt = s1 + s2
  def max(s1: SInt, s2: SInt): SInt = Mux(s1 > s2, s1, s2)
  def min(s1: SInt, s2: SInt): SInt = Mux(s1 < s2, s1, s2)
  // TODO: I think this may not be very robust, but its only used once we've verified that at least
  //       one value is >= min_possible, so we should be safe
  def clipped_min(s1: SInt, s2: SInt, min_ex: SInt): SInt = Mux(s1 < s2, Mux(s1 > min_ex, s1, s2), Mux(s2 > min_ex, s2, s1))

  // Define modules
  val actors = Vec(Seq.fill(row_num){ Module(new actor_module(W, row_size, coefficient_n)).io })
  
  // Define useful registers
  // Track pivot row and column indices
  val pivot_col = Reg(UInt(W.W))
  val pivot_row = Reg(UInt(W.W))
  val ratios = Reg(Vec(row_size - 1, SInt(W.W)))
  val ratios_valid = Reg(Vec(row_size - 1, UInt(W.W)))
  val min_ratio = Reg(SInt(W.W))

  // Pulled from Ximing's Sha3 solution, this keeps data from being overwritten
  // But I think it must result in extra memory usage UNLESS Chisel optimizes this well
  val actor_data_in = Reg(Vec(row_num, Vec(row_size, SInt(W.W))))

  // Default connections
  for(i <- 0 until row_num) {
    actors(i).message.bits := actor_data_in(i)
    actors(i).message.valid := false.B
    actors(i).print_content := false.B
    actors(i).send_min_coef := false.B
    actors(i).get_value := false.B
    actors(i).get_value_index := 0.U
    actors(i).compute_ratio := false.B
    actors(i).pivot_col := 0.U
    actors(i).step_4 := false.B
    actors(i).step_5 := false.B
  }

  io.message.ready := false.B
  io.optimal_value.bits := 0.S
  io.optimal_value.valid := false.B

  // Define the FSM
  val s_idle :: s_read :: s_pivot_col :: s_pivot_row :: s_write :: s_s4 :: s_s5 :: Nil = Enum(7)
  val state = RegInit(init = s_idle)
  io.busy := !(state === s_idle)

  when (state === s_idle) {
    // If idle we can report the contents of the rows
    when (io.print_content) {
      // printf(p"Row ${io.print_content_row}\n")
      actors(io.print_content_row).print_content := true.B
    }
    
    // If idle then we are ready to read data
    io.message.ready := true.B
    when (io.message.valid) {
      state := s_read
    }

    // Start signal starts the whole process
    when (io.start_simplex) {
      state := s_pivot_col
    }
  }

  // Step 1. Set up tableau
  // read data and write directly to the rows
  when (state === s_read) {
    for(i <- 0 until row_num) {
      when (io.message.valid && actors(i).message.ready) {
        printf(p"${io.message.bits(i)}\n")
        actor_data_in(i) := io.message.bits(i)
        actors(i).message.valid := true.B
      }
    }
    state := s_idle
  }

  // Step 2. Search for pivot column
  when (state === s_pivot_col) {
    // Pivot column is just the min col on the bottom row
    // Let that row know we need its min value
    actors(row_num.U - 1.U).send_min_coef := true.B

    // Wait for a response, then store it and move to the next state
    when(actors(row_num.U - 1.U).min_coef_valid) {
      printf(p"min_coef: ${actors(row_num.U - 1.U).min_coef}\n")
      printf(p"min_coef_index: ${actors(row_num.U - 1.U).min_coef_index}\n")

      when(actors(row_num.U - 1.U).min_coef >= 0.S) {
        // If min_coef value is non-negative then exit
        printf("No pivot column!\n")
        state := s_write
      } .otherwise {
        // Otherwise save pivot col proceed to choosing the pivot row
        pivot_col := actors(row_num.U - 1.U).min_coef_index
        printf(p"Pivot column: ${actors(row_num.U - 1.U).min_coef_index}\n")
        state := s_pivot_row
      }
    } .otherwise {
      // Stay in this state while we wait
      state := s_pivot_col
    }
  }

  // Step 3. Search for pivot row
  when (state === s_pivot_row) {
    // Don't include the constraint row
    for(i <- 0 until (row_num-1)) {
      actors(i).compute_ratio := true.B
      actors(i).pivot_col := pivot_col
      when (actors(i).ratio_valid === 1.U) {
        printf(p"ratio ${i}: ${actors(i).ratio}\n")
        ratios(i) := actors(i).ratio
        ratios_valid(i) := actors(i).ratio_valid
      }
    }

    val max_ratio_temp = Wire(SInt(W.W))
    val min_ratio_temp = Wire(SInt(W.W))
    min_ratio_temp := 0.S
    max_ratio_temp := 0.S
    when (ratios_valid.reduce(_ + _) === row_num.U - 1.U) {
      // If all ratios are non-positive then there is no pivot row
      max_ratio_temp := ratios.reduceLeft{(acc, value) => max(acc, value)}
      when (max_ratio_temp <= 0.S) {
        printf("No pivot row!\n")
        state := s_write
      } .otherwise {
        // Otherwise choose the pivot row
        // We want the min positive ratio
        min_ratio_temp := ratios.reduceLeft{(a, b) => clipped_min(a, b, 0.S)}
        // As noted above, clipped_min might not be very robust, but because
        // we only call it after verifying there is at least one valid response
        // it should be safe.
        min_ratio := min_ratio_temp
        pivot_row := ratios.indexWhere((p) => p === min_ratio_temp)
        state := s_s4
      }
    } .otherwise {
      state := s_pivot_row
    }
  }
  
  // Step 4. Pivot the pivot row
  when (state === s_s4) {
    printf(p"Pivot row: ${pivot_row}\n")
    actors(pivot_row).step_4 := true.B
    actors(pivot_row).pivot_col := pivot_col
    // Wait for pivot to complete
    when(actors(pivot_row).step_4_valid) {
      state := s_s5
    } .otherwise {
      state := s_s4
    }
  }

  // Step 5. Pivot everything else (elementar)
  when (state === s_s5) {
    for(i <- 0 until row_num) {
      // Skip pivot row
      when (i.U === pivot_row) {
        // do nothing
      } .otherwise {
        actors(i).step_5 := true.B
        actors(i).pivot_col := pivot_col
        // TODO: Send pivot row data
        // TODO: Wait for valid response
      }
    }
  }
  // Step 6. Return
  // Write the optimal value and return to idle
  when (state === s_write) {
    // Get the optimal value
    actors(row_num.U - 1.U).get_value := true.B
    actors(row_num.U - 1.U).get_value_index := row_size.U - 1.U
    // Wait for optimal value
    when(actors(row_num.U - 1.U).the_value_is_valid) {
      io.optimal_value.valid := true.B
      io.optimal_value.bits := actors(row_num.U - 1.U).the_value
      state := s_idle
    } .otherwise {
      state := s_write
    }
  }

  printf(p"state: ${state}\n")
}
