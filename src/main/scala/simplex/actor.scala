package simplex

import chisel3._
import chisel3.util._
//import dsptools._

class actor_module(val W: Int = 64, val row_size: Int = 6, val coefficient_n: Int = 2)
extends Module {
  val io = IO(new Bundle {
    // Row data
    val message = Flipped(Decoupled((Vec(row_size, SInt(W.W)))))
    
    // Send min coefficient and index 
    val send_min_coef = Input(Bool())
    val min_coef_valid = Output(Bool())
    val min_coef = Output(SInt(W.W))
    val min_coef_index = Output(UInt(W.W))

    // Send value at row index
    val get_value = Input(Bool())
    val get_value_index = Input(UInt(W.W))
    val the_value_is_valid = Output(Bool())
    val the_value = Output(SInt(W.W))
    
    // Compute ratio
    val compute_ratio = Input(Bool())
    val pivot_col = Input(UInt(W.W))
    // Ratio valid is a fake boolean because I need to do math with it
    val ratio_valid = Output(UInt(W.W))
    val ratio = Output(SInt(W.W))

    // step 4
    val step_4 = Input(Bool())
    val step_4_valid = Output(Bool())

    // steo 5
    // val pivot_row = Input(Vec(row_size, SInt(W.W)))
    val step_5 = Input(Bool())
    val step_5_valid = Output(Bool())

    // Debug
    val print_content = Input(Bool())
  })

  // Helper functions
  def max(s1: SInt, s2: SInt): SInt = Mux(s1 > s2, s1, s2)
  def min(s1: SInt, s2: SInt): SInt = Mux(s1 < s2, s1, s2)
  def clipped_min(s1: SInt, s2: SInt, min_ex: SInt): SInt = Mux(s1 < s2, Mux(s1 > min_ex, s1, s2), Mux(s2 > min_ex, s2, s1))

  // Row data
  val row_data = Reg(Vec(row_size, SInt(W.W)))

  // Default connections
  io.message.ready := false.B  
  io.min_coef := 0.S
  io.min_coef_index := 0.U
  io.min_coef_valid := false.B
  io.the_value_is_valid := false.B
  io.the_value := 0.S
  io.ratio_valid := 0.U
  io.ratio := 0.S
  io.step_4_valid := false.B
  io.step_5_valid := false.B

  // Define the FSM
  val s_idle :: s_read :: s_print :: s_get_value :: s_compute_ratio :: s_send_min_coef :: s_s4 :: s_s5 :: Nil = Enum(8)
  val state = RegInit(init = s_idle)

  when (state === s_idle) {
    // If idle we can respond to commands from the director
    when(io.print_content) {
      state := s_print
    }

    when(io.get_value) {
      state := s_get_value
    }

    io.message.ready := true.B
    when (io.message.valid) {
      state := s_read
    }

    when (io.send_min_coef) {
      state := s_send_min_coef
    }

    when (io.compute_ratio) {
      state := s_compute_ratio
    }

    when (io.step_4) {
      state := s_s4
    }

    when (io.step_5) {
      state := s_s5
    }
  }

  when (state === s_print) {
    printf(p"${row_data}\n")
    state := s_idle
  }

  when (state === s_get_value) {
    io.the_value_is_valid := true.B
    io.the_value := row_data(io.get_value_index)
    state := s_idle
  }

  // ...read data
  when (state === s_read) {
    row_data := io.message.bits
    state := s_idle
  }

  // Send min coef and min coef position
  when (state === s_send_min_coef) {
    // Use some wires to avoid copy-paste
    val min_coef = Wire(SInt(W.W))
    val min_coef_index = Wire(UInt(W.W))
    min_coef := 0.S
    min_coef_index := 0.U

    // Row data includes both coef and slack values, we only want to look at coefs
    // Note that slice is inclusive
    min_coef := row_data.slice(0, coefficient_n).reduceLeft{(v1, v2) => min(v1, v2)}
    io.min_coef := min_coef
    // We want to return the leftmost min val index, in the case of ties
    // which is what indexWhere will do per the Chisel3 docs
    // Not slicing here, but this _shouldn't_ return a non-coef because of the above
    io.min_coef_index := row_data.indexWhere((p) => p === min_coef)
    io.min_coef_valid := true.B
    state := s_idle
  }

  // Compute ratios
  when (state === s_compute_ratio) {
    io.ratio := row_data(row_size.U-1.U)/row_data(io.pivot_col)
    io.ratio_valid := 1.U
    state := s_idle
  }

  when (state === s_s4) {
    printf(p"Pivoting the pivot row..\n")
    for (i <- 0 until row_size) {
      row_data(i) := row_data(i)/row_data(io.pivot_col)
    }
    io.step_4_valid := true.B
    state := s_idle
  }

  when (state === s_s5) {
    printf(p"Pivoting everything else..\n")
    for (i <- 0 until row_size) {
      // row_data(i) := row_data(i) - row_data(io.pivot_col)*io.pivot_row(i)
    }
    io.step_5_valid := true.B
    state := s_idle
  }
}