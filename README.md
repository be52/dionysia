# Dionysia

Dionysia is a [Chisel](https://www.chisel-lang.org/) implementation of the [Simplex](https://en.wikipedia.org/wiki/Simplex) algorithm for solving linear programming problems. The architecture is inspired by the [Actor](https://en.wikipedia.org/wiki/Actor_model) concurrency model.

## Modules

The `director_module` module is the controller, it dispatches data to the other modules and issues commands. The `actor_module` holds and operates on normal rows. The `cost_module` holds and operates on the cost row.

## The Name

The name [_Dionysia_](https://en.wikipedia.org/wiki/Dionysia) comes from an ancient Greek theatrical festival.